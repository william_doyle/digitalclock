/**
 * 
 */

/**
 * @author wdd
 *
 */

/*
 			0
 		1		2
 			3	
 		4		5
 			6
 			
 		**My Standard
 		
 		
 */
public class SevenSegmentDisplay implements Updateable, Clearable, Printable{

	private String pl(String s, int n) {
		return String.format("%" + n + "s", s);	
	}
	private String pr(String s, int n) {
		return String.format("%-" + n + "s", s);
		
	}
	
	private int hPR = 3; // horizontal pad right
	
	private int _value;
	
	public boolean _lights [] = { false, false, false, false, false, false, false };
	
	public int getValue() {
		return _value;
	}
	
	public void setValue(int v) {
		if ((v >= 0) && (v <= 9)) {
			this._value = v;
		}
		else {
			System.out.println("[Bad] Cannot put this number on the SSD");
		}
	}
	
	@Override
	public void update() {
		//this.clear();
		switch (this._value) {
		case 0:
			// set the zero set of lights
			for (int i = 0; i < 7; i++) {
				if (i != 3 ) {
					_lights[i] = true;
				}
			}
			break;
		case 1:
			// set the zero set of lights
			_lights[2] = true;
			_lights[5] = true;
			break;
		case 2:
			// set the zero set of lights
			_lights[0] = true;
			_lights[2] = true;
			_lights[3] = true;
			_lights[4] = true;
			_lights[6] = true;
			break;
		case 3:
			// set the zero set of lights
			_lights[0] = true;
			_lights[2] = true;
			_lights[3] = true;
			_lights[5] = true;
			_lights[6] = true;
			break;
		case 4:
			// set the zero set of lights
			_lights[1] = true;
			_lights[2] = true;
			_lights[3] = true;
			_lights[5] = true;
			break;
		case 5:
			// set the zero set of lights
			_lights[0] = true;
			_lights[1] = true;
			_lights[3] = true;
			_lights[5] = true;
			_lights[6] = true;
			break;
		case 6:
			// set the zero set of lights
			for (int i = 0; i < 7; i++) {
				if (i != 2 ) {
					_lights[i] = true;
				}
			}
			break;
		case 7:
			// set the zero set of lights
			_lights[0] = true;
			_lights[2] = true;
			_lights[5] = true;
			break;
		case 8:
			// set the zero set of lights
			for (int i = 0; i < 7; i++) {
				_lights[i] = true;
			}
			break;
		case 9:
			// set the zero set of lights
			for (int i = 0; i < 7; i++) {
				if (i != 4 ) {
					_lights[i] = true;
				}
			}
			break;
		}
	}

	@Override
	public void clear() {
		for (int i = 0; i < 7; i++) {
			_lights[i] = false;
		}
		
	}
	
	public void print_row1() {
		if (this._lights[0] == true) {
			System.out.print(pl(" --", hPR+2)+ " ");
		}
		else {
			System.out.print(pl(" ", hPR+2) + " ");
		}
	}
	
	// name print_row2()
	// author William Doyle
	// purpose print the correct chars to display the state of leds 1 and 2 
	public void print_row2() {
		if (this._lights[1] == true) {
			System.out.print(pl("|", hPR));
		}
		else {
			System.out.print(pl(" ", hPR));
		}
		if (this._lights[2] == true) {
			System.out.print(pl("|", 3));
		}
		else {
			System.out.print(pl(" ", hPR));
		}
	}
	
	public void print_row3() {
		if (this._lights[3] == true) {
			System.out.print(pl(" --", hPR+2)+" ");
		}
		else {
			System.out.print(pl(" ", hPR+2)+ " ");
		}
	}
	
	public void print_row4() {
		if (this._lights[4] == true) {
			System.out.print(pl("|", hPR));
		}
		else {
			System.out.print(pl(" ", hPR));
		}
		if (this._lights[5] == true) {
			System.out.print(pl("|", 3));
		}
		else {
			System.out.print(pl(" ", 3));
		}
	}
	
	public void print_row5() {
		if (this._lights[6] == true) {
			System.out.print(pl(" --", hPR+2)+ " ");
		}
		else {
			System.out.print(pl(" ", hPR+2)+ " ");
		}
	}

	@Override
	public void print() {
		// Print the first row of the four 
//		if (_lights[0]) {
//			System.out.println("  -- ");
//		}
//		else {
//			System.out.println("");
//		}
//		
//		if (_lights[1]) {
//			System.out.print(" |");
//		}
//		else {
//			System.out.print("  ");
//		}
//		if (_lights[2]) {
//			System.out.println("  |");
//		}
//		else {
//			System.out.println("");
//		}
//		if (_lights[3]) {
//			System.out.println("  --- ");
//		}
//		else {
//			System.out.println("");
//		}
//		if (_lights[4]) {
//			System.out.print(" |");
//		}
//		else {
//			System.out.print("  ");
//		}
//		// here...
//		if (_lights[5]) {
//			System.out.println("  |");
//		}
//		else {
//			System.out.println("");
//		}
//		
//		if (_lights[6]) {
//			System.out.println("  -- ");
//		}
//		else {
//			System.out.println("");
//		}
		
	}
	
}
