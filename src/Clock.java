import java.util.Calendar;

/**
 * 
 */

/**
 * @author wdd
 *
 */
public class Clock implements Runnable, Updateable, Clearable, Printable{
	public SevenSegmentDisplay disArr [] = { new SevenSegmentDisplay(), new SevenSegmentDisplay(), new SevenSegmentDisplay(), new SevenSegmentDisplay()  };
	
	int tens_hour = 0;
	int ones_hour = 0;
	int tens_minute = 0;;
	int ones_minute;
	
	int hours = 0;
	int minutes = 0;
	
	
//	Clock () {
//		for (int i = 0; i < 4; i++) {
//			this.disArr[i] = new SevenSegmentDisplay();
//		}
//	}
	
	public void refreshNumerals() {
		// Calculate the correct numeral
		if (hours > 9) {
			tens_hour = 1;
			ones_hour = hours - 10;
		}
		else {
			tens_hour = 0;
			ones_hour = hours;
		}
		if (minutes > 9) {
			tens_minute = minutes / 10;
			ones_minute = minutes - ((minutes / 10) * 10);
		}
		else {
			tens_minute = 0;
			ones_minute = minutes;
		}
		// pass values to corisponding SevenSegmentDisplays
		disArr[0].setValue(tens_hour);
		disArr[1].setValue(ones_hour);
		disArr[2].setValue(tens_minute);
		disArr[3].setValue(ones_minute);
		
	}
	
	public void getTime() {
		Calendar cal = Calendar.getInstance();
		this.hours = cal.get(Calendar.HOUR);
		this.minutes = cal.get(Calendar.MINUTE);
		//System.out.println("hours: " + hours + "\nminutes: " + minutes);
	}
	
	/*
	 * Author William Doyle
	 * Purpose clear all the values of the internal SevenSegmentDisplays
	 */
	@Override
	public void clear() {
		for (SevenSegmentDisplay ssd : disArr) {
			ssd.clear();
		}
	}

	@Override
	public void update() {
		
		disArr[0].setValue(tens_hour);
		disArr[0].update();
		
		disArr[1].setValue(ones_hour);
		disArr[1].update();
		
		disArr[2].setValue(tens_minute);
		disArr[2].update();
		
		disArr[3].setValue(ones_minute);
		disArr[3].update();
		
	}

	public void print_row1() {
		for (SevenSegmentDisplay ssd : disArr) {
			ssd.print_row1();
		}
		System.out.println("");
	}
	public void print_row2() {
		for (SevenSegmentDisplay ssd : disArr) {
			ssd.print_row2();
		}
		System.out.println("");
	}
	public void print_row3() {
		for (SevenSegmentDisplay ssd : disArr) {
			ssd.print_row3();
		}
		System.out.println("");
	}
	public void print_row4() {
		for (SevenSegmentDisplay ssd : disArr) {
			ssd.print_row4();
		}
		System.out.println("");
	}
	public void print_row5() {
		for (SevenSegmentDisplay ssd : disArr) {
			ssd.print_row5();
		}
		System.out.println("");
	}
	
	@Override
	public void run() {
		// Get time
		this.getTime();
		// clear screen
		this.clear();
		// translate time to the four seven segemnt display
		this.refreshNumerals();
		// use the ssdisplay objects to display the time
		this.update();
		this.print();
	}

	@Override
	public void print() {
		
		this.print_row1();
		this.print_row2();
		this.print_row3();
		this.print_row4();
		this.print_row5();
		
		for (int i = 0; i < 16; i++) {
			System.out.print("-");
		}
		System.out.println("");
		
	}

}
