/**
 * 
 */

/**
 * @author wdd
 *
 */
public interface Clearable {
	public void clear();
}
